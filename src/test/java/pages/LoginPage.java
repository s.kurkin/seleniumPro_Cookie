package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//a[@class='navbar-link fedora-navbar-link']")
    private WebElement logInMainButton;

    @FindBy(xpath = "//input[@class='btn btn-primary btn-md login-button']")
    private WebElement logInButton;

    @FindBy(id = "user_email")
    private WebElement userEmailElement;

    @FindBy(id = "user_password")
    private WebElement userPasswordElement;

    @FindBy(xpath = "//a[@class='fedora-navbar-link navbar-link dropdown-toggle open-my-profile-dropdown']")
    private WebElement userSettings;

    @FindBy(xpath = "//li[@class='user-signout']/a")
    private WebElement logOutButton;

    public LoginPage(RemoteWebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void logInMainClick() {
        clickElement(logInMainButton);
    }

    public void logInClick() {
        clickElement(logInButton);
    }

    public void enterUserEmail(String userEmail) {
        sendKeysToElement(userEmailElement, userEmail);
    }

    public void enterPassword(String userPassword) {
        sendKeysToElement(userPasswordElement, userPassword);
    }

    public void userSettingsClick() {
        clickElement(userSettings);
    }

    public void logOutClick() {
        clickElement(logOutButton);
    }

    public boolean isLogIn() {
        return new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(userSettings)).isEnabled();
    }
}
