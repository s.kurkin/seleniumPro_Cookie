package cases;

import com.google.gson.*;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.core.io.ClassPathResource;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class CookieCase {

    private final String COOKIE_PAGE_URL = "http://courses.way2automation.com/";

    private String tmpLoginPageURL;
    private String userPassword;
    private String userEmail;

    private RemoteWebDriver driver;
    private WebDriverWait wait;
    private LoginPage loginPage;

    @BeforeClass
    public void setUp() throws Exception {
        ChromeDriverManager.getInstance().version("2.29").setup();
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        initProperties();
    }

    @Test
    public void testCookie() throws IOException {
        driver.get(COOKIE_PAGE_URL);
        logIn();
        logOut();
        logInWithCookie();
        Assert.assertTrue(loginPage.isLogIn());
    }

    private void initProperties() throws IOException {
        Properties config = new Properties();
        try {
            config.load(new ClassPathResource("config.properties").getInputStream());
        } catch (IOException ex) {
            ex.printStackTrace();
            fail("File 'config.properties' can't be open!");
        }
        userEmail = config.getProperty("cookie.user.email");
        userPassword = config.getProperty("cookie.user.password");
    }

    private void logInWithCookie() throws IOException {
        JsonArray arrayCookies = readCookie();
        driver.navigate().to(tmpLoginPageURL);
        saveCookieToDriver(arrayCookies);
        driver.navigate().refresh();
    }

    private JsonArray readCookie() throws IOException {
        JsonArray arrayCookies = new JsonArray();
        try {
            BufferedReader br = new BufferedReader(new FileReader(new ClassPathResource("Cookie.txt").getFilename()));
            arrayCookies = new JsonParser().parse(br).getAsJsonArray();
        } catch (IOException e) {
            e.printStackTrace();
            fail("Can't read file Cookie.txt");
        }
        return arrayCookies;
    }

    private void saveCookieToDriver(JsonArray arrayCookies) {

        for (JsonElement elementCookie : arrayCookies) {
            JsonObject objCookie = (JsonObject) elementCookie;
            if (objCookie != null
                    && objCookie.get("domain") != null
                    && objCookie.get("expiry") != null
                    && (objCookie.get("domain").toString().contains("way2automation.com") || objCookie.get("domain").toString().contains("teachable.com"))) {
                Cookie cookie = new Cookie(
                        StringUtils.mid(objCookie.get("name").toString(), 1, objCookie.get("name").toString().length() - 2)
                        , StringUtils.mid(objCookie.get("value").toString(), 1, objCookie.get("value").toString().length() - 2)
                        , StringUtils.mid(objCookie.get("domain").toString(), 1, objCookie.get("domain").toString().length() - 2)
                        , StringUtils.mid(objCookie.get("path").toString(), 1, objCookie.get("path").toString().length() - 2)
                        , DateUtils.addDays(new Date(), 1));
                driver.manage().addCookie(cookie);
            }
        }
    }

    private void logOut() {
        loginPage.userSettingsClick();
        loginPage.logOutClick();
    }

    private void logIn() throws IOException {
        loginPage = new LoginPage(driver);
        loginPage.logInMainClick();
        loginPage.enterUserEmail(userEmail);
        loginPage.enterPassword(userPassword);
        loginPage.logInClick();
        tmpLoginPageURL = driver.getCurrentUrl();
        saveCookieToFile();
    }

    private void saveCookieToFile() throws IOException {
        Set<Cookie> allCookies = driver.manage().getCookies();
        String jsonCookies = new Gson().toJson(allCookies);
        FileWriter fileWriter = new FileWriter(new ClassPathResource("Cookie.txt").getFilename());
        try {
            fileWriter.write(jsonCookies);
        } catch (IOException e) {
            e.printStackTrace();
            fail("File 'Cookie.txt' can't be saved!");
        } finally {
            fileWriter.flush();
            fileWriter.close();
        }
    }

    @AfterClass
    public void tearDown() throws Exception {
        driver.close();
        driver.quit();
    }
}
