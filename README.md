Задание:
 1) Написать функцию записи куков в файл и считывания куков из него.
 2) Написать тест, авторизующийся на сайте http://courses.way2automation.com/ с помощью формы авторизации при первом запуске
 и использующий куки при втором
     
Запуск теста:	 
1) В файле \src\test\resources\config.properties заполнить проперти соответствующими значениями:  

cookie.user.email 
cookie.user.password

2) mvn clean -Dtest=CookieCase test